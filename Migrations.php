<?php

/**
 * Здесь будут собраны нужные для повседневного использования
 * скрипты и команды для создания миграций в Yii2
 */

// Миграция  с добавлением столбцов
// php yii migrate/create add_columns_to_user_profile_table --fields="birth_date:date,city:string(55)"
// php yii migrate/create add_columns_to_user_table --fields=”bonus_level:integer,bonuses:integer"
// php yii migrate/create add_columns_to_epu_log_table --fields="datetime_event_timestamp:integer:unsigned"

// Создание таблиц с определенными полями и настройками.
// php yii migrate/create create_epu_command_queue_table --fields="hash_sum:string(255),ident:integer,command_type_num:string(255),status:tinyInteger,result:string(255),created_at:integer:unsigned,created_by:integer:foreignKey(user),updated_at:string(255),deleted:tinyInteger:defaultValue(0)"
// php yii migrate/create create_guide_epu_command_table --fields="type_num:integer,type:string(255)"

// Добавление полей в базу пачкой
const TABLE_NAME = 'Название таблицы';

$commands = [
            ['Значение 1', 'Значение 2'],
            ['Значение 1', 'Значение 2'],
            ['Значение 1', 'Значение 2'],
            ['Значение 1', 'Значение 2'],
            ['Значение 1', 'Значение 2'],
            ['Значение 1', 'Значение 2'],
        ];

$this->batchInsert(self::TABLE_NAME, ['Столбец 1', 'Столбец 2'], $commands);

// Обновление полей через цикл в миграции
$logs = EpuLog::findAll(['datetime_event_timestamp' => null]);

foreach ($logs as $log) {
    $this->update('{{%epu_log}}',
    ['datetime_event_timestamp' => strtotime($log->datetime_event)],
    ['id' => $log->id]);
}

// или через SQL
$connection = Yii::$app->getDb();

$command = $connection->createCommand("
    UPDATE epu_log SET datetime_event_timestamp = UNIX_TIMESTAMP(datetime_event)
    ",
    ['datetime_event_timestamp' => null]);

$command->queryAll();
