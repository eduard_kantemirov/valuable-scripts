# ![Эдуард Кантемиров](https://gitlab.com/uploads/-/system/user/avatar/9681906/avatar.png?width=40) Эдуард Кантемиров 
Backend-разработчик. PHP, Yii2, MySQL, Git

💬 [Telegram](https://t.me/kantemirov_eduard) - для связи со мной. \
👔 [Профиль на Харб.Карьере](https://career.habr.com/eduard-kantemirov) \
🐱‍💻 [LinkedIn](https://www.linkedin.com/in/eduard-kantemirov) 

## Курсы / тренажеры которые прохожу
🏆 [CodeWars профиль](https://www.codewars.com/users/eduard-kantemirov) \
🏆 [SQL тренажер](https://learndb.ru/check-cert/0e41bfde-5b52-4517-aca1-9a628906adc5) \
🏆 [Hexlet](https://ru.hexlet.io/u/kantemirov-eduard) \
🏆 [Дмитрий Елисеев - Неделя ООП](https://elisdn.ru/oop-week)
🏆 Прошел [курс по PHPStorm](https://masteringphpstorm.com/)


## Для чего этот репозиторий?

Здесь я буду собирать свои рукописные скрипты, которые помогали или помогают мне делать свою работу. \
Плюс это отличная практика для меня, для документирования проектов. \
Я пишу на PHP и использую на данный момент фреймворк Yii2. 

## Ссылки

- [x] [Helpers](https://gitlab.com/eduard_kantemirov/valuable-scripts/-/blob/main/Helpers.php) - базовые скрипты, которые я выношу отдельно. 
- [x] [Migrations](https://gitlab.com/eduard_kantemirov/valuable-scripts/-/blob/main/Migrations.php) - команды для миграций, чтобы было проще их находить или писать новые.
- [x] [Query](https://gitlab.com/eduard_kantemirov/valuable-scripts/-/blob/main/Query.php) - мои функции, для расширения функциональности поиска и запросов к базе. 
- [x] [Docker](https://gitlab.com/eduard_kantemirov/valuable-scripts/-/blob/main/domains.md) - заметки по докеру. 
- [x] [PHPStorm](https://gitlab.com/eduard_kantemirov/valuable-scripts/-/blob/main/PHPStorm.md) - заметки по phpstorm. 

## Планы

- [ ] Собрать в отдельном репозитории проект на Yii2, в портфолио.
- [ ] Добавить в этот репозиторий базовые конструкции из ООП
- [ ] Добавить алгоритмы в этот репозиторий
