Прошел [курс по PHPStorm](https://masteringphpstorm.com/)

## Горячие клавиши которыми я пользуюсь
1. Клик колесом мыши - переход в метод или если сделать обратно, покажет где метод используется в проекте.
2. Alt + S (в стандарте Ctrl + F12) - показать структуру файла, показывает доступные методы.
3. Ctrl + Ctrl + вниз/вверх - многострочное выделение, позволяет быстро править несколько строчек
4. Shift + 6 - переименовать метод/переменную сразу во всем файле или проекте
5. Alt + W - закрыть все лишние окна
6. Ctrl + Shift + e - редко использую, показывает где я до этого был, с возможностью перейти туда
7. Alt+Alt (удерживаем) - показывает tool window которые можно скрыть и показывать когда нужно.
8. Ctrl + E - recent files which I change
9. Ctrl + E -> Shift + Enter - выбираешь файл из последних и открываешь в формате сплит экрана
10. Shift + A - настроил на опцию Unsplit All

## Можно настроить для себя Postfix Completion
возможность редактировать код, по ходу дела по шаблону.
Написал 
$user
приписал к нему .if
и получил готовую конструкцию

if ($user) {

}

## Если настроить Live Template 
можно оборачивать свой код другими конструкциями, если часто их используешь.
