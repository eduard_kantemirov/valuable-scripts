<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name  Имя клиента
 * @property integer $phone Телефон клиента
 * */

class Customer extends Model
{

    protected $guarded = [];
}
