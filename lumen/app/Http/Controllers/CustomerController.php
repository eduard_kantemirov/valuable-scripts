<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class CustomerController extends Controller
{
    public function index(): JsonResponse {
        return response()->json(Customer::all());
    }

    public function show(int $id): JsonResponse {
        $customer = Customer::query()->find($id);
        return response()->json($customer ?: 'Клиент не найден');
    }

    public function create(Request $request): JsonResponse {
        $customer = new Customer();

        $customer->name = $request->name ?: 'Имя не заполнено';
        $customer->phone = $request->phone ?: 'Телефон отсутствует';
        $customer->save();

        return response()->json('Клиент успешно создан');
    }

    public function update(Request $request, int $id): JsonResponse {
        $customer = Customer::query()->find($id);
        if (!empty($customer)) {
            $customer->name = $request->name ?: $customer->name;
            $customer->phone = $request->phone ?: $customer->phone;
            $customer->save();
        }
        return response()->json($customer ?: 'Клиент не найден');
    }

    public function delete($id): JsonResponse {
        $customer = Customer::query()->find($id);
        if ($customer && $customer->delete()) {
            return response()->json('Клиент успешно удален');
        }
        return response()->json('Клиент не найден');
    }
}
