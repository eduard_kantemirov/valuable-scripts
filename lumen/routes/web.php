<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', fn() => $router->app->version());
$router->get('/customer', 'CustomerController@index');
$router->get('/customer/{id}', 'CustomerController@show');
$router->post('/customer', 'CustomerController@create');
$router->put('/customer/{id}', 'CustomerController@update');
$router->delete('/customer/{id}', 'CustomerController@delete');
