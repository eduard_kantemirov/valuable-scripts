<?php

/**
 * Собраны варианты расширения основных Query для Yii2.
 */

/**
 * Необходимо отправить необходимый возраст клиентов. <br>
 * Если прислать оба значения - найдет клиентов Между. <br>
 * Если прислать только "от" - найдет клиентов от этого полного возраста и выше. <br>
 * Если прислать только "до" - найдет клиентов от этого полного возраста и ниже. <br>
 * @param null $age_from - начальный возраст "от".
 * @param null $age_until - конечный возраст "до".
 * @return $this
 */
public function fullAge($age_from = null, $age_until = null): UserQuery {
    if (empty($age_from) && empty($age_until)) {
        return $this;
    }
    $this->joinWith('userProfile');
    if (!empty($age_from) && !empty($age_until)) {
        $this->addSelect(['user.*',
                          "DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(user_profile.birth_date, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(user_profile.birth_date, '00-%m-%d')) as age"])
            ->andHaving(['BETWEEN', 'age', $age_from, $age_until])
            ->groupBy('user.id');
    } elseif (!empty($age_from)) {
        $this->addSelect(['user.*',
                          "DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(user_profile.birth_date, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(user_profile.birth_date, '00-%m-%d')) as age"])
            ->andHaving(['>=', 'age', $age_from])
            ->groupBy('user.id');
    } elseif (!empty($age_until)) {
        $this->addSelect(['user.*',
                          "DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(user_profile.birth_date, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(user_profile.birth_date, '00-%m-%d')) as age"])
            ->andHaving(['<=', 'age', $age_until])
            ->groupBy('user.id');
    }
    return $this;
}

/**
 * Поиск последнего входа в систему. В случае если отправить оба, ищет между датами.
 * @param null $logged_at_from - формат даты, если передать только эту переменную, то будет искать ОТ нее.
 * @param null $logged_at_until - формат даты, если передать только эту переменную, то будет искать ДО нее.
 * @return $this
 */
public function lastLogin($logged_at_from = null, $logged_at_until = null): UserQuery {
    if (empty($logged_at_from) && empty($logged_at_until)) {
        return $this;
    }
    if (!empty($logged_at_from) && !empty($logged_at_until)) {
        $this->andFilterWhere(['BETWEEN', User::col('logged_at'), strtotime($logged_at_from),
                               strtotime($logged_at_until)]);
    } elseif (!empty($logged_at_from)) {
        $this->andFilterWhere(['>=', User::col('logged_at'), strtotime($logged_at_from)]);
    } elseif (!empty($logged_at_until)) {
        $this->andFilterWhere(['<=', User::col('logged_at'), strtotime($logged_at_until)]);
    }
    return $this;
}

/**
 * Функция поиска по дате регистрации пользователя. В случае если отправить оба, ищет между датами.
 * @param null $created_at_from - формат даты, если передать только эту переменную, то будет искать ОТ нее.
 * @param null $created_at_until - формат даты, если передать только эту переменную, то будет искать ДО нее.
 * @return $this
 */
public function registrationDate($created_at_from = null, $created_at_until = null): UserQuery {
    if (empty($created_at_from) && empty($created_at_until)) {
        return $this;
    }
    if (!empty($created_at_from) && !empty($created_at_until)) {
        $this->andFilterWhere(['BETWEEN', User::col('created_at'), strtotime($created_at_from),
                               strtotime($created_at_until)]);
    } elseif (!empty($created_at_from)) {
        $this->andFilterWhere(['>=', User::col('created_at'), strtotime($created_at_from)]);
    } elseif (!empty($created_at_until)) {
        $this->andFilterWhere(['<=', User::col('created_at'), strtotime($created_at_until)]);
    }
    return $this;
}

/**
 * Передаем id типа топлива, получаем на выходе отфильтрованных
 * пользователей, которые делали заказ подобного топлива.
 * @param string|null $fuelType
 * @return $this
 */
public function usersFromFuelType(string $fuelType = null) {
    if (empty($fuelType) || is_array($fuelType)) {
        return $this;
    }
    $this->joinWith('userOrders');
    $this->leftJoin('tariff_scale', 'orders.tariff_scale_id = tariff_scale.id');
    $this->leftJoin('fuel_type', 'tariff_scale.fuel_id = fuel_type.id');
    $this->andFilterWhere([TariffScale::col('fuel_id') => $fuelType]);
    return $this;
}

/**
 * Передаем строку с id клиентами, на выходе дополняем фильтр по клиентам
 * @param $clientIdArr
 * @return $this
 */
public function clientIdArr($clientIdArr) {
    if (empty($clientIdArr)) {
        return $this;
    }
    $clientArr = explode(',', $clientIdArr);
    $clientArr = array_map('trim', $clientArr);
    $this->andFilterWhere(['IN', User::col('id'), $clientArr]);
    return $this;
}
