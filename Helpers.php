<?php

/**
 * Можно использовать в Yii2
 * @param array|null $arr array
 * @param string|null $separator string. По умолчанию - пробел
 * @return string|null
 * Сливает воедино массив данных с разделением пробелом.
 * Если будет пустое значение - удалит его из массива.
 */
public static function implodeCleanArray(?array $arr, ?string $separator = ' '): string {
    $infoArr = array_map('trim', $arr);
    return implode($separator, array_filter($infoArr, function ($v) {
        return !empty($v);
    }));
}

/**
 * Можно использовать в Yii2
 * @param $timestamp
 * @return string
 * @throws \yii\base\InvalidConfigException
 * Кидаем ему таймстамп, в ответ получаем строку с датой и временем разделенной <br>
 */
public static function dateTimeTwoRow($timestamp): string {
    return Yii::$app->formatter->asTime($timestamp, "php:d.m.Y")
        . '<br>' . Yii::$app->formatter->asTime($timestamp, "php:H:i");
}

/**
 * Можно использовать в Yii2
 * @param $birth_date - высылаем дату рождения в формате timestamp
 * @return string
 * @throws \yii\base\InvalidConfigException
 */
public static function userAge($birth_date) {
    if (empty($birth_date)) {
        return '';
    }
    $diff     = new DateTime($birth_date);
    $interval = $diff->diff(new DateTime('now'));
    return $interval->format('%Y лет');
}

/**
 * Начало суток для указанной метки времени
 * @param $timestamp
 * @param string $timezone
 * @return int
 * @throws \Exception
 */
public static function startTimeOfTheDay($timestamp, string $timezone = "UTC"): int {
    $date = new DateTime("now", new DateTimeZone($timezone));
    $date->setTimestamp($timestamp);
    $date->setTime(0, 0);
    return $date->getTimestamp();
}

/**
 * @param $phone - пересылаем поле телефон, в зависимости от того,
 * пустое оно или нет будет выведена разная маска для телефона.
 * По умолчанию пустое значение.
 * @return string
 */

// в проекте потребовалось менять шаблон маски телефона
// в зависимости от того - редактируем или создаем пользователя.

public static function phoneMask($phone = null): string {
    return (!empty($phone) ? '+9' : '+7') . ' (999) 999 99 99';
}


/**
 * Передаем в метод роль пользователя, в зависимости, от роли сделает запись.
 * админ, супер-админ, оператор - null
 * физ лицо, юр лицо, водитель - соответствующую запись
 * Если неизвестная роль или отсутствующая - запись как физ лица.
 * @param array|string $roleFromModel
 * @return mixed|string|null
 */

// К сожалению данную функцию пришлось писать из-за наличия поля myrole в старом проекте.
// В дополнении к обычным ролям пользователя.

public static function setUserMyrole($roleFromModel = []) {
    !is_string($roleFromModel) ?: $roleFromModel = array($roleFromModel);
    // массив ролей менеджеров, чтобы прекратить сразу выводить NULL вместо значения myrole
    $managerRoles = [
        Role::ROLE_SUPER_ADMINISTRATOR,
        Role::ROLE_ADMINISTRATOR,
        Role::ROLE_OPERATOR,
    ];

    // массив ролей клиентов и водителя, чтобы записать в myrole нужное значение.
    $roles = [
        Role::ROLE_PERSON  => User::STATUS_INDIVID,
        Role::ROLE_COMPANY => User::STATUS_COMPANY,
    ];

    // проходим, проверяем, есть ли в переданных ролях нужная роль у пользователя,
    // если есть, то возвращаем спец наименование в myrole,
    // если натыкаемся на роль управляющих - возвращаем NULL
    foreach ($roleFromModel as $roleString) {
        if (in_array($roleString, $managerRoles)) { return null; }
        if (in_array($roleString, $roles)) { return $roles[$roleString]; }
    }

    // если так выйдет, что в переданных данных нет ничего, то возвращает в myrole значение individ.
    return User::STATUS_INDIVID;
}

// если потребуется поменять пароль пользователю в Yii2
// 

/**
* Generates password hash from password and sets it to the model
* @param string $password
*/
	public function setPassword($password) {
		$this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
	}

    // и уже после Можно
    // Чтобы сгенерировать новый пароль пользователю.

$user = User::findOne();
$user->setPassword(1234);
$user->save();
