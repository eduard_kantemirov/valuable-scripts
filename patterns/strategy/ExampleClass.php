<?php

/**
 * В этом классе мы принимаем в псевдо варианте строку с названием работника.
 * И выполняем в зависимости от названия нужные действия.
 */
class ExampleClass {

    public function index(string $orderForWork) {
        return match ($orderForWork) {
            'designer'   => (new Company(new Designer()))->doWork(),
            'programmer' => (new Company(new Programmer()))->doWork(),
            default      => "We don't have such workers",
        };
    }
}