<?php

interface EmployeeInterface {

    /**
     * @return string
     */
    public function doWork(): string;

    /**
     * @return string
     */
    public function getJobName(): string;
}