<?php

class Programmer implements EmployeeInterface {

    /**
     * @inheritDoc
     */
    public function doWork(): string {
        return "Work like " . self::class;
    }

    /**
     * @inheritDoc
     */
    public function getJobName(): string {
        return self::class;
    }
}