<?php

class Designer implements EmployeeInterface {

    public const JOB_NAME = 'Designer';

    /**
     * @inheritDoc
     */
    public function doWork(): string {
        return "Work like " . self::JOB_NAME;
    }

    /**
     * @inheritDoc
     */
    public function getJobName(): string {
        return self::JOB_NAME;
    }
}