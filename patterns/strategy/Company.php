<?php

/**
 * Паттерн Стратегия, позволяет сделать основной класс не зависимым от других классов.
 * И позволяет ему быть расширяемым.
 * Без внесения изменений в основной класс.
 */
class Company {

    private EmployeeInterface $employeeType;

    public function __construct(EmployeeInterface $employeeType) {
        $this->employeeType = $employeeType;
    }

    /**
     * @param EmployeeInterface $employeeType
     */
    public function setEmployeeType(EmployeeInterface $employeeType): void {
        $this->employeeType = $employeeType;
    }

    public function doWork() {
        return $this->employeeType->doWork();
    }
}